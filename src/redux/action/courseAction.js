export const counterTypes={
    VUECOURSECHANGE: "VUECOURSECHANGE",
    ANGULARCOURSECHANGE: "ANGULARCOURSECHANGE"
    JAVACOURSECHANGE: "JAVACOURSECHANGE"
}


export const vue = () => {
    return {
        type:counterTypes.VUECOURSECHANGE
    }
}

export const angular = () => {
    return {
        type:counterTypes.ANGULARCOURSECHANGE
    }
}
export const java = () => {
    return {
        type:counterTypes.JAVACOURSECHANGE
    }
}