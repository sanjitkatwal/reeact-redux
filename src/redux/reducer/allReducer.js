import {combineReducers} from "redux";
import counterReducer from "./counterReducer";
import switchReducer from "./switchReducer";
import cakeReducer from "./cakeReducer"

const allReducer = combineReducers({
    counter: counterReducer,
    switch : switchReducer,
    cake   : cakeReducer,
})



export default allReducer