import {counterTypes} from "../action/courseAction"
const initState = 'select one';

const counterReducer=(state=initState, action)=>{
    switch (action.type){
        case counterTypes.VUECOURSECHANGE:
            return state + 1;
        case counterTypes.DECREMENT:
            return state - 1;

        default:
            return state // or initState
    }
}

export default counterReducer