import {cakeTypes} from "../action/cakeAction"
const initialState = {
    numOfCakes : 10
};

const cakeReducer=(state=initialState, action)=>{
    switch (action.type){
        case cakeTypes.BUY_CAKE: return {
            ...state,
            numOfCakes: state.numOfCakes - 1
        }
        /*case cakeTypes.BUY_CAKE:
            return state.numOfCakes - 1;*/

        default:
            return state // or initState
    }
}

export default cakeReducer