import React from "react";
import './App.css';
import Counter from '../src/component/Counter'
import Switch from  '../src/component/Switch'
import CourseSelector from '../src/component/CourceSelector'
import HooksCakeContainr from '../src/component/HooksCakeContainer'
function App() {
  return (
    <div className="App">
      <b>Redux Simplified Example</b>
        <br />
        <Counter />
        <Switch />
      <br />
      <CourseSelector />
        <hr />
        <HooksCakeContainr />

    </div>
  );
}

export default App;
