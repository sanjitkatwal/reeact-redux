import React from 'react'
import {useSelector, useDispatch} from 'react-redux'
import {buyCake} from '../redux/action/cakeAction'


function HooksCakeContainer(){
    const cake = useSelector((state) => state.cake.numOfCakes)
    const dispathch = useDispatch()

    return(
        <div>
            <h2>Number of Cakes - {cake}</h2>
            <button onClick={() =>{
                dispathch(buyCake())
            }}>Buy Cake</button>
        </div>
    )
}


export default HooksCakeContainer