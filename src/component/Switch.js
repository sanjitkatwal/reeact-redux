import React from 'react';
import {enableSwitch, disableSwitch} from "../redux/action/switchAction";
import {useDispatch, useSelector} from "react-redux";

const Switch = () =>{
    const switchx = useSelector(state=>state.switch)
    const dispatch = useDispatch();
    return (
        <div className='count'>
            <h3>Switch: {switchx.enabled ? "enabled" : "Disabled"}</h3>
            <button onClick={()=>{dispatch(enableSwitch())}} className='btn btn-success btn-sm m-1'>Enable</button>
            <button onClick={()=>{dispatch(disableSwitch())}}  className='btn btn-success btn-sm'>Dissable</button>

        </div>
    )
}

export default Switch