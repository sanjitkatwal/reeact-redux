import React, {useState} from 'react'


const CourceSelector = () => {
    let [state, setState] = useState({
        courseName : ''
    })

    let setCourse = (name) => {
        setState({
            courseName: name
        })
    }
    return (
        <React.Fragment>
            <div className="mt-3">
                <div className="container">
                    <div className="row">
                        <div className="col-md-5">
                            <div className="card shadow-lg">
                                <div className="card-header bg-teal text-white">
                                    <p className="h4">Course selector</p>
                                </div>
                                <div className="card-body">
                                    <p className="h3">I Enrool for: {state.courseName}</p>
                                    <button
                                        onClick={setCourse.bind(this, 'Vue JS')}
                                        className="btn btn-success m-1">Vue Js</button>
                                    <button
                                        onClick={setCourse.bind(this, 'Java')}
                                        className="btn btn-primary m-1">Java Js</button>
                                    <button
                                        onClick={setCourse.bind(this, 'Angular')}
                                        className="btn btn-primary">Angular</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    );
}

export default CourceSelector;
