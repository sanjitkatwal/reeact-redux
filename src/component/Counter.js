import React from 'react';
import {decrement, increment} from "../redux/action/counterAction";
import {useDispatch, useSelector} from "react-redux";

let Counter =() =>{
    const counter = useSelector(state=>state.counter)
    const dispatch = useDispatch();
    return (
        <div className='count'>
            <h3>Counter: {counter}</h3>
            <button onClick={()=>{dispatch(increment())}} className='btn btn-success btn-sm m-1'>Increment</button>
            <button onClick={()=>{dispatch(decrement())}}  className='btn btn-success btn-sm'>Decrement</button>

        </div>
    )
}

export default Counter